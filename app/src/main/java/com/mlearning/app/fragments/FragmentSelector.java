package com.mlearning.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.mlearning.app.R;
import com.mlearning.app.activities.ResultActivity;
import com.mlearning.app.activities.ResultNilaiActivity;
import com.mlearning.app.activities.UploadActivity;
import com.mlearning.app.activities.base.BaseFragment;
import com.mlearning.app.api.ApiModule;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.MakulModel;
import com.mlearning.app.models.MessageModel;
import com.mlearning.app.models.Pusher;
import com.mlearning.app.models.SemesterModel;
import com.mlearning.app.models.TugasModel;
import com.mlearning.app.util.Constans;
import com.mlearning.app.util.Gxon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

import static com.mlearning.app.activities.MainActivity.TIPE_MATERI;
import static com.mlearning.app.activities.MainActivity.TIPE_NILAI_TUGAS;
import static com.mlearning.app.activities.MainActivity.TIPE_TAB;
import static com.mlearning.app.activities.MainActivity.TIPE_TUGAS;

/**
 * Created by asepahmad
 * Date: 3/27/19
 * Project: ELearningAkakom
 */
public class FragmentSelector extends BaseFragment {

    @BindView(R.id.spMakul)
    AppCompatSpinner spMakul;
    @BindView(R.id.spSemester)
    AppCompatSpinner spSemester;
    @BindView(R.id.spTugas)
    AppCompatSpinner spTugas;
    @BindView(R.id.btnTampil)
    Button btnTampil;
    @BindView(R.id.lytSemester)
    View lytSemester;
    @BindView(R.id.lytTugas)
    View lytTugas;


    private ArrayAdapter<MakulModel> makulAdapter;
    private ArrayAdapter<SemesterModel> semesterAdapter;
    private ArrayAdapter<TugasModel> tugasAdapter;
    private List<MakulModel> dMakul= new ArrayList<>();
    private MakulModel dMakulSelected = new MakulModel();
    private List<SemesterModel> dSemester = new ArrayList<>();
    private SemesterModel dSemesterSelected = new SemesterModel();
    private List<TugasModel> dTugas = new ArrayList<>();
    private TugasModel dTugasSelected = new TugasModel();
    private PrefManager prefManager;
    private String TIPE = "";
    private String PARAM = "";

    public static FragmentSelector newInstances(String tipe) {

        FragmentSelector f = new FragmentSelector();
        Bundle args = new Bundle();
        args.putString(TIPE_TAB, tipe);
        f.setArguments(args);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_selector, container, false);
        ButterKnife.bind(this, v);
        EventBus.getDefault().register(this);
        prefManager = new PrefManager(getContext());
        TIPE = getArguments().getString(TIPE_TAB);
        dMakul.add(new MakulModel("", "Pilih Makul"));
        dSemester.add(new SemesterModel("", "Semester"));
        initView();


        return v;
    }

    private void initView(){
        lytTugas.setVisibility(TIPE.equals(TIPE_NILAI_TUGAS) && prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE).equals(Constans.STATUS_DOSEN) ? View.VISIBLE:View.GONE);
        makulAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, dMakul);
        spMakul.setAdapter(makulAdapter);
        spMakul.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dMakulSelected = dMakul.get(position);
                if(!prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE).equals(Constans.STATUS_MHS))getTugas(dMakulSelected.getKodeMk());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tugasAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, dTugas);
        spTugas.setAdapter(tugasAdapter);
        spTugas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dTugasSelected = dTugas.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE).equals(Constans.STATUS_MHS)) {
            getSemester();
            semesterAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, dSemester);
            spSemester.setAdapter(semesterAdapter);
            spSemester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    dSemesterSelected = dSemester.get(position);
                    getMakul(dSemesterSelected.getId_semester());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }else {
            lytSemester.setVisibility(View.GONE);
            getMakulDsn();
        }

        btnTampil.setOnClickListener(view -> {
            if(!dMakulSelected.getKodeMk().equals("")){
                if(TIPE.equals(TIPE_NILAI_TUGAS)){
                    if(prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE).equals(Constans.STATUS_MHS))
                        ResultNilaiActivity.go(ctx, Gxon.to(dMakulSelected));
                    else
                        ResultNilaiActivity.go(ctx, Gxon.to(dMakulSelected), Gxon.to(dTugasSelected));
                }else if(TIPE.equals(TIPE_MATERI) || TIPE.equals(TIPE_TUGAS)){
                    if(prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE).equals(Constans.STATUS_MHS))
                        ResultActivity.to(getActivity(), TIPE, Gxon.to(dMakulSelected));
                    else
                        UploadActivity.go(getActivity(), TIPE, Gxon.to(dMakulSelected));
                }
            }else toast("Pilih matakulaih");

        });
    }

    private void getMakul(String id_semester){
        if(id_semester.equals("")){
            defaultMakul();
        }else {
            showLoading();
            disposable.add(
                    ApiModule
                            .getInstance()
                            .getMakulBySmt(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID), id_semester)
                            .subscribe(d -> {
                                dMakul.addAll(d);
                                hideLoading();
                            }, t -> {
                                toast(t.getMessage());
                                hideLoading();
                            })
            );
        }
    }
    //untuk dosen
    private void getTugas(String idMakul){
        if(idMakul.equals("")){
            defaultTugas();
        }else {
            showLoading();
            disposable.add(
                    ApiModule
                            .getInstance()
                            .getTugasDosen(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID), idMakul)
                            .subscribe(d -> {
                                dTugas.addAll(d);
                                hideLoading();
                            }, t -> {
                                toast(t.getMessage());
                                hideLoading();
                            })
            );
        }
    }
    private void getMakulDsn(){
        showLoading();
        Map<String, String> param = new HashMap<>();
        param.put("nip", prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID));
        disposable.add(
                ApiModule
                        .getInstance()
                        .getMakul(param)
                        .subscribe(d -> {
                            dMakul.addAll(d);
                            hideLoading();
                        }, t -> {
                            toast(t.getMessage());
                            hideLoading();
                        })
        );
    }


    private void getSemester(){
        disposable.add(
                ApiModule
                        .getInstance()
                        .getSemester(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID))
                        .subscribe(d -> {
                            if(d.size()==0){
                                defaultMakul();
                                return;
                            }
                            dSemester.addAll(d);
                        }, t -> {
                            toast(t.getMessage());
                        })
        );
    }

    private void defaultMakul(){
        dMakul.clear();
        dMakul.add(new MakulModel("", "Pilih Makul"));
        makulAdapter.notifyDataSetChanged();

    }
    private void defaultTugas(){
        dTugas.clear();
        dTugas.add(new TugasModel("", "Pilih Tugas"));
        tugasAdapter.notifyDataSetChanged();

    }

    @SuppressWarnings("unused")
    public void onEvent(Pusher pusher) {
        if (Constans.KEY_SELECTOR.equals(pusher.action)) {
            TIPE = pusher.data;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

}
