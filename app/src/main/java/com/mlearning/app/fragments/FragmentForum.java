package com.mlearning.app.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.mlearning.app.R;
import com.mlearning.app.activities.ForumActivity;
import com.mlearning.app.activities.base.BaseFragment;
import com.mlearning.app.api.ApiModule;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.MakulModel;
import com.mlearning.app.util.Constans;
import com.mlearning.app.util.Gxon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mlearning.app.activities.MainActivity.TIPE_TAB;

/**
 * Created by asepahmad
 * Date: 3/27/19
 * Project: ELearningAkakom
 */
public class FragmentForum extends BaseFragment {

    @BindView(R.id.spMakul)
    AppCompatSpinner spMakul;
    @BindView(R.id.btnTampil)
    Button btnTampil;

    private ArrayAdapter<MakulModel> makulAdapter;
    private List<MakulModel> data = new ArrayList<>();
    private MakulModel dataSelected = new MakulModel();
    private PrefManager prefManager;

    public static FragmentForum newInstances() {
        FragmentForum f = new FragmentForum();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_forum, container, false);
        ButterKnife.bind(this, v);
        prefManager = new PrefManager(ctx);
        makulAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, data);
        data.add(new MakulModel("", "Makul"));
        spMakul.setAdapter(makulAdapter);
        getMakul();
        spMakul.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dataSelected = data.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //menampilkan activity room chat
        btnTampil.setOnClickListener(view -> {
            if(dataSelected.getKodeMk().equals("")){
                toast("Pilih matakuliah");
            }else ForumActivity.go(ctx, Gxon.to(dataSelected));
        });
        return v;
    }

    //menampilkan data matakuliah
    private void getMakul(){
        showLoading();
        Map<String, String> param = new HashMap<>();
        param.put(Constans.STATUS_MHS.equals(prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE)) ? "nim" : "nip", prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID));
        disposable.add(
                ApiModule
                        .getInstance()
                        .getMakul(param)
                        .subscribe(d -> {
                            data.addAll(d);
                            hideLoading();
                        }, t -> {
                            toast(t.getMessage());
                            hideLoading();
                        })
        );
    }
}
