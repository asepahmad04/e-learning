package com.mlearning.app.activities;

import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.mlearning.app.R;
import com.mlearning.app.db.PrefManager;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 2000;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prefManager = new PrefManager(this);

        new Handler().postDelayed(() -> {
            if(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID)!=null){
                MainActivity.go(this);
                finish();
            }else {
                LoginActivity.go(this);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
