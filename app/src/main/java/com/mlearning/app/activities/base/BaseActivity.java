package com.mlearning.app.activities.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

//class sebagai base dari setiap activity
public class BaseActivity extends AppCompatActivity
{
    protected ProgressDialog dialog;
    protected CompositeDisposable disposable;
    public BaseActivity baseContext;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        baseContext = this;
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        disposable = new CompositeDisposable();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        //menghapus wadah operasi yang ada pada rxjava
        if (!disposable.isDisposed()) disposable.dispose();
    }

    //fungsi menampilkan dialog
    public void showDialogs(String title, String msg)
    {
        dialog.setTitle(title);
        dialog.setMessage(msg);
        dialog.show();
    }
    //fungsi menyembunyikan dialog
    protected void hideDialog()
    {
        dialog.hide();
    }

    //menampilkan toast
    protected void message(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    //menampilkan alert dialog
    protected void messageA(String msg)
    {
        new AlertDialog.Builder(this)
                .setTitle("Msg")
                .setMessage(msg)
                .setPositiveButton("OK", null)
                .show();
    }
}
