package com.mlearning.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mlearning.app.R;
import com.mlearning.app.activities.base.BaseActivity;
import com.mlearning.app.api.ApiModule;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.UserModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mlearning.app.util.Constans.STATUS_DOSEN;
import static com.mlearning.app.util.Constans.STATUS_MHS;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.edUser)
    EditText edUser;
    @BindView(R.id.edPass)
    EditText edPass;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    private PrefManager prefManager;

    //intent dari activity
    public static void go(Context _ctx) {
        Intent i = new Intent(_ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        _ctx.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);

    }

    //aksi login mahasiswa
    @OnClick(R.id.btnLogin)
    public void doLoginMhs(){
        actLogin(STATUS_MHS);
    }

    //aksi login dosen
    @OnClick(R.id.btnLoginDosen)
    public void doLoginDosen(){
        actLogin(STATUS_DOSEN);
    }

    //mengirim data login ke server berdasarkan status
    private void actLogin(String tipe){

        String user = edUser.getText().toString().trim();
        String pass = edPass.getText().toString().trim();
        if(user.equals("")){
            edUser.setError("Masukkan username");
        }else if(pass.equals("")){
            edPass.setError("Masukkan password");
        }else {
            showDialogs("", "Silahkan tunggu...");
            disposable.add(
                    ApiModule
                            .getInstance()
                            .actLogin(user, pass, tipe)
                            .subscribe(m -> {
                                hideDialog();
                                if(m.status){
                                    //menyimpan session
                                    UserModel d = m.getResponse();
                                    prefManager.setSession(tipe.equals(STATUS_MHS) ? d.getNim() : d.getNip(),d.getPassword(), d.getUsername(), d.getNama(), d.getJk(), d.getProdi(), tipe);
                                    MainActivity.go(baseContext);
                                    finish();
                                }else {
                                    message(m.message);
                                }

                            }, t -> {
                                message(t.getMessage());
                                hideDialog();
                            })
            );
        }

    }
}
