package com.mlearning.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.mlearning.app.R;
import com.mlearning.app.activities.base.BaseActivity;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.util.Constans;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity {
    @BindView(R.id.tID)
    TextView tID;
    @BindView(R.id.tvID)
    TextView tvId;
    @BindView(R.id.tvUser)
    TextView tvUser;
    @BindView(R.id.tvNama)
    TextView tvNama;
    @BindView(R.id.tvJK)
    TextView tvJK;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ic_back)
    ImageView icBack;


    private PrefManager prefManager;

    public static void go(Context c){
        Intent i = new Intent(c, ProfileActivity.class);
        c.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        tvTitle.setText("Profile");
        tID.setText(Constans.STATUS_MHS.equals(prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE)) ? "NIM" : "NIP");
        tvId.setText(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID));
        tvUser.setText(prefManager.getUserDetails().get(PrefManager.PREF_KEY_USER_NAME));
        tvNama.setText(prefManager.getUserDetails().get(PrefManager.PREF_KEY_NAME));
        tvJK.setText("L".equals(prefManager.getUserDetails().get(PrefManager.PREF_KEY_JK)) ? "Laki-laki" : "Perempuan");
        tvStatus.setText(Constans.STATUS_MHS.equals(prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE)) ? "Mahasiswa" : "Dosen");

        icBack.setOnClickListener(v -> finish());
    }
}
