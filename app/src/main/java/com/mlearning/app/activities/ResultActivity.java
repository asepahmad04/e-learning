package com.mlearning.app.activities;

import android.Manifest;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mlearning.app.R;
import com.mlearning.app.activities.base.BaseActivity;
import com.mlearning.app.adapters.MateriAdapter;
import com.mlearning.app.adapters.TugasAdapter;
import com.mlearning.app.api.ApiEndpoints;
import com.mlearning.app.api.ApiModule;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.Download;
import com.mlearning.app.models.MakulModel;
import com.mlearning.app.util.Gxon;
import com.mlearning.app.util.PermissionHandler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mlearning.app.activities.MainActivity.MESSAGE_PROGRESS;
import static com.mlearning.app.activities.MainActivity.TIPE_TUGAS;

public class ResultActivity extends BaseActivity implements PermissionHandler.PermissionCallback {

    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.web_view)
    WebView webView;
    @BindView(R.id.lyt_no_item)
    View lytNoItem;
    @BindView(R.id.swiper)
    SwipeRefreshLayout swiper;

    private MateriAdapter materiAdapter;
    private TugasAdapter tugasAdapter;
    private String tipe = "materi ";
    private MakulModel makulModel;

    public static void to(Context context, String tipe, String param){
        Intent i = new Intent(context, ResultActivity.class);
        i.putExtra("tipe",tipe);
        i.putExtra("param",param);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi);
        ButterKnife.bind(this);
        if (PermissionHandler.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && PermissionHandler.checkPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE))
            initView();
        else
            PermissionHandler.requestMultiPermission(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PermissionHandler.REQUEST_CODE_CHOOSE);


    }

    private void initView(){
        makulModel = Gxon.from(getIntent().getStringExtra("param"), MakulModel.class);
        //inisialisasi webview
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            final String data_file = URLUtil.guessFileName(url, contentDisposition, mimetype);
            final AlertDialog.Builder builder = new AlertDialog.Builder(baseContext);
            builder.setTitle("Download");
            builder.setMessage("Anda ingin mengunduh "+data_file+" ?");
            builder.setPositiveButton("Ya", (dialog, which) -> {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                DownloadManager DM = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                String cookies = CookieManager.getInstance().getCookie(url);
                request.setMimeType(mimetype);
                request.addRequestHeader("cookies", cookies);
                request.addRequestHeader("uerAgent", userAgent);
                request.setTitle(data_file);
                request.setAllowedOverRoaming(false);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI|DownloadManager.Request.NETWORK_MOBILE);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.allowScanningByMediaScanner();
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, data_file);
                if (DM != null) {
                    DM.enqueue(request);
                }
                Toast.makeText(getApplicationContext(), "Downloading File", Toast.LENGTH_LONG).show();

            });
            builder.setNegativeButton("Tidak", (dialog, which) -> dialog.cancel());
            AlertDialog dialog = builder.create();
            dialog.show();
            hideDialog();
        });

        getData();
        swiper.setOnRefreshListener(this::getData);
    }

    //ambil data
    private void getData(){
        if(getIntent().getStringExtra("tipe").equals(TIPE_TUGAS)){
            tipe = "tugas ";
            getTugas();
        } else {
            getMateri();
        }
    }

    //mengambil data tugas
    private void getTugas(){
        swiper.setRefreshing(true);
        tugasAdapter = new TugasAdapter(this, new ArrayList<>());
        list.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        list.setAdapter(tugasAdapter);
        tugasAdapter.setOnItemClickListener(model -> {
            startDownload(model.getNamaFile());
            showDialogs("", "Silahkan tunggu...");
        });
        disposable.add(
                ApiModule
                        .getInstance()
                        .getTugas(makulModel.getKodeMk(), makulModel.getKelas())
                        .subscribe(d -> {
                            if(d.size() > 0){
                                tugasAdapter.setItem(d);
                                showNoItem(false);
                            }else showNoItem(true);
                        }, t -> {
                            message(t.getMessage());
                            showNoItem(true);
                        })
        );
    }

    //mengambil data materi
    private void getMateri(){
        swiper.setRefreshing(true);
        materiAdapter = new MateriAdapter(this, new ArrayList<>());
        list.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        list.setAdapter(materiAdapter);
        materiAdapter.setOnItemClickListener(model -> {
            startDownload(model.getNamaFile());
        });
        disposable.add(
                ApiModule
                        .getInstance()
                        .getMateri(makulModel.getKodeMk(), makulModel.getKelas())
                        .subscribe(d -> {
                            if(d.size() > 0){
                                materiAdapter.setItem(d);
                                showNoItem(false);
                            }else showNoItem(true);
                        }, t -> {
                            message(t.getMessage());
                            showNoItem(true);
                        })
        );
    }

    //menampilkan view saat tidak ada item pada list
    private void showNoItem(boolean show){
        swiper.setRefreshing(false);
        list.setVisibility(show ? View.GONE:View.VISIBLE);
        lytNoItem.setVisibility(show ? View.VISIBLE:View.GONE);

    }

    //mengunduh data meteri/tugas
    private void startDownload(String name) {
        String pre = tipe.equals(MainActivity.TIPE_MATERI) ? "materi/" : "tugas/";
        final String url = ApiEndpoints.BASE_FILE+ pre +name;
        webView.loadUrl(url);
    }


    //jika permission diizinkan
    @Override
    public void Granted() {
        initView();
    }

    //jika permission ditolak
    @Override
    public void Refused(String message) {
        message(message);
        finish();
    }
}
