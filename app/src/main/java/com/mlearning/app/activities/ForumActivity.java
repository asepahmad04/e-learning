package com.mlearning.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mlearning.app.R;
import com.mlearning.app.activities.base.BaseActivity;
import com.mlearning.app.adapters.ChatAdapter;
import com.mlearning.app.api.ApiModule;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.MakulModel;
import com.mlearning.app.models.MessageModel;
import com.mlearning.app.models.Pusher;
import com.mlearning.app.util.Constans;
import com.mlearning.app.util.Gxon;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

import static com.mlearning.app.util.Constans.STATUS_MHS;

public class ForumActivity extends BaseActivity {

    private static final String PARAM_KD_MAKUL = "PARAM_KD_MAKUL";
    private static final String PARAM_KELAS = "PARAM_KELAS";

    @BindView(R.id.list_chat)
    RecyclerView list_chat;
    @BindView(R.id.et_message)
    EditText edMsg;
    @BindView(R.id.icSend)
    ImageView icSend;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.loader)
    ProgressBar loader;
    @BindView(R.id.ic_back)
    ImageView icBack;

    private MakulModel room;
    private MessageModel roomFcm;
    private String pKodeMakul;
    private String pKelas;
    private PrefManager prefManager;
    private ChatAdapter chatAdapter;
    private List<MessageModel> messages;

    //intent dari activity
    public static void go(Context _ctx, String kd_makul) {
        Intent i = new Intent(_ctx, ForumActivity.class);
        i.putExtra(PARAM_KD_MAKUL, kd_makul);
        _ctx.startActivity(i);
    }

    //intent dari notifikasi
    public static Intent fromNotif(Context _ctx, String kd_makul) {
        Intent i = new Intent(_ctx, ForumActivity.class);
        i.putExtra("fcm", kd_makul);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        EventBus.getDefault().register(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list_chat.setLayoutManager(layoutManager);
        list_chat.setHasFixedSize(true);
        changeIconMsg(false);

        //menampilkan data yang berasal dari intent
        if (getIntent().hasExtra(PARAM_KD_MAKUL)) {
            room = Gxon.from(getIntent().getStringExtra(PARAM_KD_MAKUL), MakulModel.class);
            pKodeMakul = room.getKodeMk();
            pKelas = room.getKelas();
            if(room.getNamaMk()!=null)tvTitle.setText(room.getNamaMk());
            getMessage();
        }

        //menampilkan data berasal dari notifikasi
        if (getIntent().hasExtra("fcm")) {
            roomFcm = Gxon.from(getIntent().getStringExtra("fcm"), MessageModel.class);
            pKodeMakul = roomFcm.kode_mk;
            pKelas = roomFcm.kelas;
            if(roomFcm.nama_mk!=null)tvTitle.setText(roomFcm.nama_mk);
            getMessage();
        }

        //listener dari edittext yang akan merubah ikon pada saat terjadi perubahan text pada edittext
        edMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty() && s.length() > 0 && !s.equals("")) {
                    changeIconMsg(true);
                } else {
                    changeIconMsg(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        icBack.setOnClickListener(v -> finish());
    }

    @OnClick(R.id.icSend)
    public void doSend() {
        if (!edMsg.getText().toString().trim().isEmpty()) addMessage();
    }

    //menampilkan data pesan dari server
    private void getMessage() {
        showLoader(true);
        disposable.add(
                ApiModule
                        .getInstance()
                        .getMessage(pKodeMakul, pKelas)
                        .subscribe(d -> {
                            if (d.status) {
                                messages = d.getResponse();
                                chatAdapter = new ChatAdapter(this, messages);
                                list_chat.setAdapter(chatAdapter);
                                list_chat.scrollToPosition(chatAdapter.getItemCount() - 1);
                            }
                            showLoader(false);
                        }, t -> {
                            changeIconMsg(true);
                            showLoader(false);
                            message(t.getMessage());
                        })
        );
    }

    //mengirim pesan berdasarkan status (mahasiswa/dosen)
    private void addMessage() {
        changeIconMsg(false);
        showLoader(true);
        HashMap<String, String> param = new HashMap<>();
        param.put("kd_makul", pKodeMakul);
        param.put("konten", edMsg.getText().toString().trim());
        param.put("kelas", pKelas);
        if(prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE).equals(STATUS_MHS)){
            param.put("status", "M");
            param.put("nim", prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID));
            param.put("nip", "");
        } else {
            param.put("status", "D");
            param.put("nip", prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID));
            param.put("nim", "");
        }

        //setelah pesan dikirim, data pesan pada activity akan diperbaharui
        disposable.add(
                ApiModule
                        .getInstance()
                        .addMessage(param)
                        .subscribe(d -> {
                            if (d.status) {
                                getMessage();
                                edMsg.setText("");
                            }
                        }, t -> {
                            changeIconMsg(true);
                            showLoader(false);
                            message(t.getMessage());
                        })
        );

    }

    //merubah icon pada tombol kirim pesan
    private void changeIconMsg(boolean active) {
        icSend.setColorFilter(active ? ContextCompat.getColor(this, R.color.colorPrimary) : ContextCompat.getColor(this, R.color.colorDarkGray), android.graphics.PorterDuff.Mode.MULTIPLY);
        icSend.setEnabled(active);
    }

    //menampilkan ikon progress pada toolbar
    private void showLoader(boolean show){
        loader.post(()-> loader.setVisibility(show ? View.VISIBLE:View.INVISIBLE));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    //listener yang akan menerima data dari fcm
    @SuppressWarnings("unused")
    public void onEvent(Pusher pusher) {
        if (Constans.KEY_UPDATE_CHAT.equals(pusher.action)) {
            MessageModel message = Gxon.from(pusher.data, MessageModel.class);
            if (!message.nim.equals(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID)))
                getMessage();
        }
    }

}
