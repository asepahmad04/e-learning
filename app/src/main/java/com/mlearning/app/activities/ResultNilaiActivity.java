package com.mlearning.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.mlearning.app.R;
import com.mlearning.app.activities.base.BaseActivity;
import com.mlearning.app.adapters.NilaiAdapter;
import com.mlearning.app.api.ApiModule;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.MakulModel;
import com.mlearning.app.models.TugasModel;
import com.mlearning.app.util.Constans;
import com.mlearning.app.util.Gxon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultNilaiActivity extends BaseActivity {

    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.ic_back)
    ImageView icBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.lyt_no_item)
    View lytNoItem;
    @BindView(R.id.swiper)
    SwipeRefreshLayout swiper;

    private NilaiAdapter nilaiAdapter;
    private PrefManager prefManager;
    private MakulModel makulModel;
    private TugasModel tugasModel;

    public static void go(Context _ctx, String makul) {
        Intent i = new Intent(_ctx, ResultNilaiActivity.class);
        i.putExtra("param_makul", makul);
        _ctx.startActivity(i);
    }

    public static void go(Context _ctx, String makul, String tugas) {
        Intent i = new Intent(_ctx, ResultNilaiActivity.class);
        i.putExtra("param_makul", makul);
        i.putExtra("param_tugas", tugas);
        _ctx.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_nilai);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        makulModel = Gxon.from(getIntent().getStringExtra("param_makul"), MakulModel.class);
        if(getIntent().hasExtra("param_tugas")){
            tugasModel = Gxon.from(getIntent().getStringExtra("param_tugas"), TugasModel.class);
            tvTitle.setText(String.format(Locale.US, "%s - %s", makulModel.getNamaMk(), tugasModel.getNamaTugas()));
        }else tvTitle.setText(makulModel.getNamaMk());


        initView();

    }

    private void initView() {
        nilaiAdapter = new NilaiAdapter(this, new ArrayList<>(), prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE));
        list.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        list.setAdapter(nilaiAdapter);
        icBack.setOnClickListener(v -> finish());
        swiper.setOnRefreshListener(this::getData);
        getData();
    }

    //mnampilkan nilai dari server
    private void getData(){
        swiper.setRefreshing(true);
        Map<String, String> param = new HashMap<>();
        param.put("kd_makul", makulModel.getKodeMk());
        if(getIntent().hasExtra("param_tugas"))
            param.put("id_tugas", tugasModel.getIdTugas());
        param.put(prefManager.getUserDetails().get(PrefManager.PREF_KEY_TIPE).equals(Constans.STATUS_MHS) ? "nim":"nip", prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID));
        disposable.add(
                ApiModule
                        .getInstance()
                        .getNilai(param)
                        .subscribe(d -> {
                            if(d.size() > 0){
                                nilaiAdapter.setItem(d);
                                showNoItem(false);
                            }else showNoItem(true);

                        }, t -> {
                            message(t.getMessage());
                            showNoItem(true);
                        })
        );
    }

    private void showNoItem(boolean show){
        swiper.setRefreshing(false);
        list.setVisibility(show ? View.GONE:View.VISIBLE);
        lytNoItem.setVisibility(show ? View.VISIBLE:View.GONE);

    }

}
