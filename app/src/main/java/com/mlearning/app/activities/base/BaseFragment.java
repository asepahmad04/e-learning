package com.mlearning.app.activities.base;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.disposables.CompositeDisposable;


public class BaseFragment extends Fragment
{
    protected Context ctx;
    ProgressDialog progressDialog;
    FragmentManager fm;
    protected CompositeDisposable disposable;

    @Override
    public void onAttach(Context c)
    {
        super.onAttach(c);
        this.ctx= c;
        progressDialog = new ProgressDialog(c);
        fm = getChildFragmentManager();
        disposable = new CompositeDisposable();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (!disposable.isDisposed())
            disposable.dispose();
    }

    public void toast(String s)
    {
        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
    }

    public void showLoading()
    {
        progressDialog.setMessage("Mengambil data...");
        if (!progressDialog.isShowing()) progressDialog.show();
    }

    public void hideLoading()
    {
        if (progressDialog.isShowing()) progressDialog.dismiss();
    }
}
