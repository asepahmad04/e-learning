package com.mlearning.app.activities;

import android.content.Context;
import android.content.Intent;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mlearning.app.R;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.fragments.FragmentForum;
import com.mlearning.app.fragments.FragmentSelector;
import com.mlearning.app.models.Pusher;
import com.mlearning.app.util.AppHelper;
import com.mlearning.app.util.Constans;
import com.mlearning.app.util.Gxon;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    public static final String MESSAGE_PROGRESS = "message_progress";

    public static final String TIPE_TAB = "TIPE_TAB";
    public static final String TIPE_FORUM = "TIPE_FORUM";
    public static final String TIPE_MATERI = "TIPE_MATERI";
    public static final String TIPE_TUGAS = "TIPE_TUGAS";
    public static final String TIPE_NILAI_TUGAS = "TIPE_NILAI_TUGAS";

    private PrefManager prefManager;
    private String token = "";

    //intent dari activity
    public static void go(Context _ctx) {
        Intent i = new Intent(_ctx, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        _ctx.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        //inisialisasi topic firebase
        FirebaseMessaging.getInstance().subscribeToTopic("room");
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            token = task.getResult().getToken();
            Log.e("FirebaseInstanceId", token);
        });
        initTab();
    }

    //inisialisasi tab menu
    private void initTab() {
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.title_forum)), true);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.title_materi)), false);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.title_tugas)), false);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.title_nilai_tugas)), false);
        open(FragmentForum.newInstances());
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.tabs_header, null);
            tabLayout.getTabAt(i).setCustomView(tv);
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        open(FragmentForum.newInstances());
                        break;
                    case 1:
                        open(FragmentSelector.newInstances(TIPE_MATERI));
                        break;
                    case 2:
                        open(FragmentSelector.newInstances(TIPE_TUGAS));
                        break;
                    case 3:
                        open(FragmentSelector.newInstances(TIPE_NILAI_TUGAS));
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    //memanggil tab menu
    private void open(Fragment f) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, f).commit();
        //AppHelper.selectFragment(R.id.container, f, "args",getSupportFragmentManager());
        //EventBus.getDefault().post(new Pusher(Constans.KEY_SELECTOR, key));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    //menampilkan menu pada toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.profile:
                ProfileActivity.go(this);
                break;
            case R.id.logout:
                prefManager.clearSession();
                LoginActivity.go(this);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Anda yakin ingin keluar dai aplikasi?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) ->finish())
                .setNegativeButton("No", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();

    }
}
