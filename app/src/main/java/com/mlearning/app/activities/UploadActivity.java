package com.mlearning.app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.jaiselrahman.filepicker.model.MediaFile;
import com.mlearning.app.R;
import com.mlearning.app.activities.base.BaseActivity;
import com.mlearning.app.adapters.MateriAdapter;
import com.mlearning.app.adapters.TugasAdapter;
import com.mlearning.app.api.ApiModule;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.MakulModel;
import com.mlearning.app.util.Gxon;
import com.mlearning.app.util.ProgressRequestbody;
import com.obsez.android.lib.filechooser.ChooserDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UploadActivity extends BaseActivity implements ProgressRequestbody.UploadCallbacks{
    private static final int FILE_REQUEST_CODE = 0x01;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvTitleFile)
    TextView tvTitleFile;
    @BindView(R.id.nmMakul)
    TextView nmMakul;
    @BindView(R.id.ic_back)
    ImageView ic_back;
    @BindView(R.id.edTitle)
    EditText edTitle;
    @BindView(R.id.tvNameFile)
    TextView tvNameFile;
    @BindView(R.id.list)
    RecyclerView listD;
    @BindView(R.id.lyt_no_item)
    View lytNoItem;
    @BindView(R.id.loader)
    ProgressBar loader;
    @BindView(R.id.swiper)
    SwipeRefreshLayout swiper;


    private String tipe = "";
    private MakulModel mMakul = new MakulModel();
    private ArrayList<MediaFile> mediaFiles = new ArrayList<>();
    private File pathF = null;
    private PrefManager prefManager;
    private ProgressDialog progressDialog;
    private MateriAdapter materiAdapter;
    private TugasAdapter tugasAdapter;

    public static void go(Context context, String tipe, String mk){
        Intent i = new Intent(context, UploadActivity.class);
        i.putExtra("tipe", tipe);
        i.putExtra("matakuliah", mk);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        progressDialog = new ProgressDialog(this);

        if(getIntent().hasExtra("tipe") && getIntent().hasExtra("matakuliah")){
            tipe = getIntent().getStringExtra("tipe");
            mMakul = Gxon.from(getIntent().getStringExtra("matakuliah"), MakulModel.class);
            if(tipe.equals(MainActivity.TIPE_MATERI)){
                tvTitle.setText("Upload Materi");
                tvTitleFile.setText("Judul Materi");
            }else {
                tvTitle.setText("Upload Tugas");
                tvTitleFile.setText("Judul Tugas");

            }

        }
        getData();
        nmMakul.setText(mMakul.getNamaMk());
        ic_back.setOnClickListener(v -> finish());
        swiper.setOnRefreshListener(this::getData);
    }

    //menmapilkan tampilan berdasarkan tipe (materi/tugas)
    private void getData(){
        if(tipe.equals(MainActivity.TIPE_MATERI))getMateri();
        else getTugas();
    }

    //mengambil data dokumen dari penyimpanan android
    @OnClick(R.id.btnPilih)
    void doPilih(){
        new ChooserDialog().with(this)
                .withFilter(false, false, "pdf","docx","doc","txt")
                .withStartFile(null)
                .withChosenListener((path, pathFile) -> {
                    pathF = pathFile;
                    tvNameFile.setText(pathFile.getName());
                })
                .build()
                .show();
    }

    @OnClick(R.id.btnUpload)
    void doUpload(){
        upload();
    }

    //fungsi upload file
    private void upload(){
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        RequestBody nm_materi = createPartFromString(edTitle.getText().toString());
        RequestBody kd_mk = createPartFromString(mMakul.getKodeMk());
        RequestBody nip = createPartFromString(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID));
        RequestBody nm_file = createPartFromString(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID));

        Map<String, RequestBody> param = new HashMap<>();
        param.put("nip", nip);
        param.put("kode_mk", kd_mk);
        param.put("nama_file", nm_file);
        param.put(tipe.equals(MainActivity.TIPE_MATERI) ? "nama_materi" : "nama_tugas", nm_materi);
        MultipartBody.Part doFile;
        //RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        ProgressRequestbody fileBody = new ProgressRequestbody(pathF, this, 0);
        doFile = MultipartBody.Part.createFormData("file", pathF.getName(), fileBody);


        //upload berdasarkan tip (materi/tugas)
        if(tipe.equals(MainActivity.TIPE_MATERI)){
            disposable.add(
                    ApiModule
                            .getInstance()
                            .upMateri(param, doFile)
                            .subscribe(d -> {
                                if(!d.status)progressDialog.dismiss();
                                else alert(d.message);
                                message(d.message);
                                getMateri();
                            }, t -> {
                                message(t.getMessage());
                                progressDialog.dismiss();
                            })
            );
        }else {
            disposable.add(
                    ApiModule
                            .getInstance()
                            .upTugas(param, doFile)
                            .subscribe(d -> {
                                if(!d.status)progressDialog.dismiss();
                                else alert(d.message);
                                message(d.message);
                            }, t -> {
                                message(t.getMessage());
                                progressDialog.dismiss();
                            })
            );
        }


    }

    private void showNoItem(boolean show){
        loader.setVisibility(View.INVISIBLE);
        swiper.setRefreshing(false);
        listD.setVisibility(show ? View.GONE:View.VISIBLE);
        lytNoItem.setVisibility(show ? View.VISIBLE:View.GONE);

    }

    private void getMateri(){
        swiper.setRefreshing(true);
        materiAdapter = new MateriAdapter(this, new ArrayList<>());
        listD.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        listD.setAdapter(materiAdapter);
        disposable.add(
                ApiModule
                        .getInstance()
                        .getMateriDosen(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID), mMakul.getKodeMk())
                        .subscribe(d -> {
                            if(d.size() > 0){
                                materiAdapter.setItem(d);
                                showNoItem(false);
                            }else showNoItem(true);

                        }, t -> {
                            message(t.getMessage());
                            showNoItem(true);
                        })
        );
    }
    private void getTugas(){
        swiper.setRefreshing(true);
        tugasAdapter = new TugasAdapter(this, new ArrayList<>());
        listD.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        listD.setAdapter(tugasAdapter);
        disposable.add(
                ApiModule
                        .getInstance()
                        .getTugasDosen(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID), mMakul.getKodeMk())
                        .subscribe(d -> {
                            if(d.size() > 0){
                                tugasAdapter.setItem(d);
                                showNoItem(false);
                            }
                            else showNoItem(true);
                        }, t -> {
                            message(t.getMessage());
                            showNoItem(true);
                        })
        );
    }

    private RequestBody createPartFromString(String partString) {
        return RequestBody.create(MediaType.parse("text/plain"), partString);
    }

    @Override
    public void onProgressUpdate(String title, int percentage) {
        progressDialog.setMessage(title);
        progressDialog.setProgress(percentage);
        progressDialog.show();
    }

    @Override
    public void onError() {
        progressDialog.dismiss();
    }

    @Override
    public void onFinish() {
        progressDialog.setProgress(0);
        progressDialog.dismiss();
    }

    private void alert(String message){
        Toast.makeText(baseContext, message, Toast.LENGTH_SHORT).show();
        if(progressDialog!=null && progressDialog.isShowing())progressDialog.dismiss();
    }
}
