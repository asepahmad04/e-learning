package com.mlearning.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asepahmad
 * Date: 5/3/19
 * Project: ELearningAkakom
 */
public class NilaiModel {
    @SerializedName("id_nilai")
    @Expose
    private String idNilai;
    @SerializedName("id_tugas")
    @Expose
    private String idTugas;
    @SerializedName("nim")
    @Expose
    private String nim;
    @SerializedName("nilai")
    @Expose
    private String nilai;
    @SerializedName("tgl")
    @Expose
    private String tgl;
    public String nama_tugas;

    public String getIdNilai() {
        return idNilai;
    }

    public void setIdNilai(String idNilai) {
        this.idNilai = idNilai;
    }

    public String getIdTugas() {
        return idTugas;
    }

    public void setIdTugas(String idTugas) {
        this.idTugas = idTugas;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }
}
