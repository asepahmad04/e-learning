package com.mlearning.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asepahmad
 * Date: 3/27/19
 * Project: ELearningAkakom
 */
public class MateriModel {
    @SerializedName("id_materi")
    @Expose
    private String idMateri;
    @SerializedName("nip")
    @Expose
    private String nip;
    @SerializedName("nama_materi")
    @Expose
    private String namaMateri;
    @SerializedName("kode_mk")
    @Expose
    private String kodeMk;
    @SerializedName("nama_file")
    @Expose
    private String namaFile;
    @SerializedName("url_file")
    @Expose
    private Object urlFile;
    @SerializedName("tgl")
    @Expose
    private String tgl;

    public String getIdMateri() {
        return idMateri;
    }

    public void setIdMateri(String idMateri) {
        this.idMateri = idMateri;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNamaMateri() {
        return namaMateri;
    }

    public void setNamaMateri(String namaMateri) {
        this.namaMateri = namaMateri;
    }

    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public Object getUrlFile() {
        return urlFile;
    }

    public void setUrlFile(Object urlFile) {
        this.urlFile = urlFile;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }
}
