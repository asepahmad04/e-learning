package com.mlearning.app.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asepahmad
 * Date: 5/3/19
 * Project: ELearningAkakom
 */
public class TugasModel {
    @SerializedName("id_tugas")
    @Expose
    private String idTugas;
    @SerializedName("nip")
    @Expose
    private String nip;
    @SerializedName("nama_tugas")
    @Expose
    private String namaTugas;
    @SerializedName("kode_mk")
    @Expose
    private String kodeMk;
    @SerializedName("nama_file")
    @Expose
    private String namaFile;
    @SerializedName("tgl")
    @Expose
    private String tgl;

    public TugasModel(){}

    public TugasModel(String id, String nama){
        this.idTugas = id;
        this.namaTugas = nama;
    }

    public String getIdTugas() {
        return idTugas;
    }

    public void setIdTugas(String idTugas) {
        this.idTugas = idTugas;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNamaTugas() {
        return namaTugas;
    }

    public void setNamaTugas(String namaTugas) {
        this.namaTugas = namaTugas;
    }

    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    @NonNull
    @Override
    public String toString() {
        return namaTugas;
    }
}
