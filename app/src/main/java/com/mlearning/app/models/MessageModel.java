package com.mlearning.app.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SplittableRandom;
import java.util.TimeZone;

/**
 * Created by asepahmad
 * Date: 3/27/19
 * Project: ELearningAkakom
 */
public class MessageModel {
    public String nim;
    public String npp_dosen;
    public String thn_akademik;
    public String nama_dosen;
    public String nama_mhs;
    public String semester;
    public String konten;
    public String tgl;
    public String tipe;
    public String id;
    public String kode_mk;
    public String status;
    public String nama_mk;
    public String kelas;

    public String getTime() {
        if (tgl == null) {
            return "now";
        } else {
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            Date date = null;
            try {
                date = serverFormat.parse(tgl);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            TimeZone timeZone = TimeZone.getDefault();
            int rawOffset = timeZone.getRawOffset();
            long local = 0;
            if (date != null) {
                local = date.getTime();
            }
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(local);
            SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.US);
            return format.format(calendar.getTime());
        }


    }

}
