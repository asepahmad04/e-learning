package com.mlearning.app.models;

public class MessageType {
    public static final int SENT_TEXT= 1;
    public static final int RECEIVED_TEXT = 2;
    public static final int RECEIVED_TEXT_DOSEN = 3;
}
