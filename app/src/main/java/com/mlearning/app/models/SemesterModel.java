package com.mlearning.app.models;

/**
 * Created by asepahmad
 * Date: 5/3/19
 * Project: ELearningAkakom
 */
public class SemesterModel {
    private String id_semester;
    private String semester;

    public SemesterModel(){}

    public SemesterModel(String id, String semester){
        this.id_semester = id;
        this.semester = semester;
    }

    public String getId_semester() {
        return id_semester;
    }

    public void setId_semester(String id_semester) {
        this.id_semester = id_semester;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @Override
    public String toString() {
        return semester;
    }
}
