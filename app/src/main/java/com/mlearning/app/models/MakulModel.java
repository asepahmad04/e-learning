package com.mlearning.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asepahmad
 * Date: 3/27/19
 * Project: ELearningAkakom
 */
public class MakulModel {
    @SerializedName("kode_mk")
    @Expose
    private String kodeMk;
    @SerializedName("nama_mk")
    @Expose
    private String namaMk;
    @SerializedName("prodi")
    @Expose
    private String prodi;
    @SerializedName("sks")
    @Expose
    private String sks;
    @SerializedName("id_peserta")
    @Expose
    private String idPeserta;
    @SerializedName("id_semester")
    @Expose
    private String idSemester;
    @SerializedName("thn_akademik")
    @Expose
    private String thnAkademik;
    @SerializedName("nip")
    @Expose
    private String nip;
    @SerializedName("kelas")
    @Expose
    private String kelas;


    public MakulModel(){}

    public MakulModel(String kd, String nama){
        this.kodeMk = kd;
        this.namaMk = nama;
    }

    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    public String getNamaMk() {
        return namaMk;
    }

    public void setNamaMk(String namaMk) {
        this.namaMk = namaMk;
    }

    public String getProdi() {
        return prodi;
    }

    public void setProdi(String prodi) {
        this.prodi = prodi;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getIdPeserta() {
        return idPeserta;
    }

    public void setIdPeserta(String idPeserta) {
        this.idPeserta = idPeserta;
    }

    public String getIdSemester() {
        return idSemester;
    }

    public void setIdSemester(String idSemester) {
        this.idSemester = idSemester;
    }

    public String getThnAkademik() {
        return thnAkademik;
    }

    public void setThnAkademik(String thnAkademik) {
        this.thnAkademik = thnAkademik;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    @Override
    public String toString() {
        return namaMk;
    }
}
