package com.mlearning.app.models;

public class Pusher {
    public String action;
    public String data;

    public Pusher(String action) {
        this.action = action;
    }

    public Pusher(String action, String data) {
        this.action = action;
        this.data = data;
    }
}
