package com.mlearning.app;

import android.app.Application;

import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by asepahmad
 * Date: 3/27/19
 * Project: ELearningAkakom
 */
public class App extends Application {
    static App mInstance;

    public static synchronized App getInstance(){
        return mInstance;
    }

    public void setmInstance(App instance){
        App.mInstance = instance;
    }
    @Override
    public void onCreate(){
        super.onCreate();
        setmInstance(this);

    }
}
