package com.mlearning.app.db;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by asepahmad
 * Date: 3/12/19
 * Project: emaklone
 */
public class PrefManager{

    private final SharedPreferences mPrefs;
    private final String prefFileName = "m_learning";
    private final SharedPreferences.Editor editor;

    public static final String PREF_KEY_USER_NAME = "PREF_KEY_USER_NAME";
    public static final String PREF_KEY_PWD = "PREF_KEY_PWD";
    public static final String PREF_KEY_ID = "PREF_KEY_ID";
    public static final String PREF_KEY_NAME = "PREF_KEY_NAME";
    public static final String PREF_KEY_JK = "PREF_KEY_JK";
    public static final String PREF_KEY_PRODI = "PREF_KEY_PRODI";
    public static final String PREF_KEY_TIPE = "PREF_KEY_TIPE";

    public static final String PREF_KEY_FCM = "PREF_KEY_FCM";


    public PrefManager(Context context){
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        editor = mPrefs.edit();
    }

    public void setSession(String id, String password, String username,
                           String name, String jk, String prodi, String tipe){
        editor.putString(PREF_KEY_ID, id);
        editor.putString(PREF_KEY_USER_NAME, username);
        editor.putString(PREF_KEY_PWD, password);
        editor.putString(PREF_KEY_NAME, name);
        editor.putString(PREF_KEY_JK, jk);
        editor.putString(PREF_KEY_PRODI, prodi);
        editor.putString(PREF_KEY_TIPE, tipe);
        editor.commit();
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<>();
        user.put(PREF_KEY_ID, mPrefs.getString(PREF_KEY_ID, null));
        user.put(PREF_KEY_USER_NAME, mPrefs.getString(PREF_KEY_USER_NAME, null));
        user.put(PREF_KEY_PWD, mPrefs.getString(PREF_KEY_PWD, null));
        user.put(PREF_KEY_NAME, mPrefs.getString(PREF_KEY_NAME, null));
        user.put(PREF_KEY_JK, mPrefs.getString(PREF_KEY_JK, null));
        user.put(PREF_KEY_PRODI, mPrefs.getString(PREF_KEY_PRODI, null));
        user.put(PREF_KEY_TIPE, mPrefs.getString(PREF_KEY_TIPE, null));
        return user;
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }
}
