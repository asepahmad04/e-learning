package com.mlearning.app.util;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;

import com.mlearning.app.App;
import com.mlearning.app.R;

/**
 * @author Rifqi Maulana onFilterClicked 16/Nov/2018
 * @project TreasueHuntNew
 */
@SuppressWarnings("unused")
public class PermissionHandler {
    public static final int REQUEST_CODE_CHOOSE = 0x01;

    /**
     * method to check for permissions
     *
     * @param activity   this is the first parameter for checkPermission  method
     * @param permission this is the second parameter for checkPermission  method
     * @return return value
     */
    public static boolean checkPermission(Activity activity, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = ContextCompat.checkSelfPermission(activity, permission);
            return result == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    /**
     * method to request permissions
     *
     * @param mActivity  this is the first parameter for requestPermission  method
     * @param permission this is the second parameter for requestPermission  method
     */
    public static void requestPermission(Activity mActivity, String permission, int request_code) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {
            String title = null;
            String Message = null;
            switch (permission) {
                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    title = mActivity.getString(R.string.storage_permission);
                    Message = mActivity.getString(R.string.write_storage_permission_message);
                    break;
                case Manifest.permission.READ_EXTERNAL_STORAGE:
                    title = mActivity.getString(R.string.storage_permission);
                    Message = mActivity.getString(R.string.read_storage_permission_message);
                    break;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setTitle(title);
            builder.setMessage(Message);
            builder.setPositiveButton(mActivity.getString(R.string.yes), (dialog, which) -> {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
                intent.setData(uri);
                mActivity.startActivity(intent);
            });
            builder.setNegativeButton(R.string.no_thanks, (dialog, which) -> dialog.dismiss());
            builder.show();
        } else {
            ActivityCompat.requestPermissions(mActivity, new String[]{permission}, request_code);

        }
    }

    public static void requestMultiPermission(Activity mActivity, String[] permission, int request_code) {
        ActivityCompat.requestPermissions(mActivity, permission,
                request_code);
    }

    public static void resultPermission(int requestCode, String[] permissions, int[] grantResults, PermissionCallback callback) {
        if (requestCode == REQUEST_CODE_CHOOSE) {
            int grant = 0;
            String Message = null;
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    grant += 1;
                } else {
                    switch (permission) {
                        case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                            Message = App.getInstance().getString(R.string.write_storage_permission_message);
                            break;
                        case Manifest.permission.READ_EXTERNAL_STORAGE:
                            Message = App.getInstance().getString(R.string.read_storage_permission_message);
                            break;
                    }
                }
            }
            if (grant == permissions.length){
                callback.Granted();
            }else{
                callback.Refused(Message);
            }
        }
    }

    public interface PermissionCallback {
        void Granted();

        void Refused(String message);
    }
}
