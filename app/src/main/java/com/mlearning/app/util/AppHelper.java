package com.mlearning.app.util;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

public class AppHelper {
    public static void selectFragment(@IdRes int layoutId, Fragment fragment, String key, FragmentManager fragmentManager) {
        fragmentManager.findFragmentByTag(fragment.getArguments().getString(key));
        FragmentTransaction ft = fragmentManager.beginTransaction();
        List<Fragment> fragment1s = new ArrayList<>();
        boolean back = false;
        if (fragmentManager.getFragments() != null)
            for (Fragment fragment1 : fragmentManager.getFragments()) {
                if (fragment1 != null) {
                    if (fragment1.getArguments() != null)
                        if (fragment.getArguments().getString(key) == fragment1.getArguments().getString(key)) {
                            fragment1s.add(fragment1);
                            back = true;
                        } else if (fragment1.getArguments().getString(key) != null) {
                            ft.hide(fragment1);
                        }
                }
            }
        if (!back) { // No fragment in backStack with same tag..
            ft.add(layoutId, fragment, fragment.getArguments().getString(key));
            ft.addToBackStack( fragment.getArguments().getString(key));
            ft.commit();
        } else {
            for (Fragment fragment1 : fragment1s){
                ft.show(fragment1);
            }
            ft.commit();
        }
    }
}
