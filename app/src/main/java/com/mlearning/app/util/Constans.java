package com.mlearning.app.util;

public class Constans {
    public static final String CHANNEL_ID = "0x001";
    public static final CharSequence CHANNEL_NAME = "mlearning channel";
    public static final String CHANNEL_DESCRIPTION = "mlearning channel description";
    public static final String KEY_UPDATE_CHAT = "KEY_UPDATE_CHAT";
    public static final String KEY_SELECTOR = "KEY_SELECTOR";
    public static final String STATUS_MHS = "mahasiswa";
    public static final String STATUS_DOSEN = "dosen";
}
