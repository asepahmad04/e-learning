package com.mlearning.app.services;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mlearning.app.R;
import com.mlearning.app.activities.ForumActivity;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.MessageModel;
import com.mlearning.app.models.Pusher;
import com.mlearning.app.util.Constans;
import com.mlearning.app.util.Gxon;

import org.json.JSONObject;

import java.util.List;

import de.greenrobot.event.EventBus;

public class FirebaseService extends FirebaseMessagingService {
    private PrefManager prefManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        prefManager = new PrefManager(getApplicationContext());
        JSONObject data = new JSONObject(remoteMessage.getData());
        Log.e("remote origin", remoteMessage.getData().toString());
        //menerima data dari fcm
        if (remoteMessage.getData().size() > 0) {
            MessageModel message = Gxon.from(data.toString(), MessageModel.class);
            String id = "M".equals(message.status) ? message.nim : message.npp_dosen;
            if (!id.equals(prefManager.getUserDetails().get(PrefManager.PREF_KEY_ID))) {
                if (isAppIsInBackground(this)) sendNotification(message);
                EventBus.getDefault().post(new Pusher(Constans.KEY_UPDATE_CHAT, Gxon.to(message)));
            }
        }

    }

    /**
     * Method check if app is in background or in foreground
     *
     * @param context this contentx
     * @return true if app is in background or false if app in foreground
     */
    //cek app berada pada background atau foreground
    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    /**
     * Method send notification
     *
     * @param message message object
     */
    private void sendNotification(MessageModel message) {
        Intent intent = ForumActivity.fromNotif(this, Gxon.to(message));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int unique_id = (int) System.currentTimeMillis();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(Constans.CHANNEL_ID, Constans.CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableLights(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, Constans.CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(android.app.Notification.DEFAULT_ALL)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(message.nama_mk)
                .setContentText(message.konten)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notificationBuilder.setPriority(android.app.Notification.PRIORITY_HIGH);
        }

        notificationManager.notify(unique_id, notificationBuilder.build());
    }




    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("FirebaseService", s);

    }
}


