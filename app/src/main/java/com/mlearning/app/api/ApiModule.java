package com.mlearning.app.api;

import com.mlearning.app.models.MakulModel;
import com.mlearning.app.models.MateriModel;
import com.mlearning.app.models.MessageModel;
import com.mlearning.app.models.NilaiModel;
import com.mlearning.app.models.SemesterModel;
import com.mlearning.app.models.TugasModel;
import com.mlearning.app.models.UserModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Multipart;

import static com.mlearning.app.api.ApiEndpoints.BASE_URL;

/**
 * Created by asepahmad
 * Date: 4/12/19
 * Project: ELearningAkakom
 */
public class ApiModule {
    private static ApiModule mInstance;

    public static ApiModule getInstance() {
        if (mInstance == null) mInstance = new ApiModule(ApiConfig.getRetrofit(BASE_URL).create(ApiService.class));
        return mInstance;
    }

    private ApiService service;

    private ApiModule(ApiService service) {
        super();
        this.service = service;
    }

    public Observable<List<MateriModel>> getMateri(String kd_mk, String kelas)
    {
        return service.getMateri(kd_mk, kelas, "data")
                .map(ApiResponse::getResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<MateriModel>> getMateriDosen(String nip, String kd_mk)
    {
        return service.getMateriDosen(nip, kd_mk)
                .map(ApiResponse::getResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<TugasModel>> getTugasDosen(String nip, String kd_mk)
    {
        return service.getTugasDosen(nip, kd_mk)
                .map(ApiResponse::getResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ApiResponse<UserModel>> actLogin(String user, String pass, String status)
    {
        return service.actLogin(user, pass, status)
                .map(data -> data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<MakulModel>> getMakul(Map<String, String> p)
    {
        return service.getMakul(p)
                .map(ApiResponse::getResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<MakulModel>> getMakulBySmt(String nim, String smt)
    {
        return service.getMakulBySmt(nim, smt)
                .map(ApiResponse::getResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<SemesterModel>> getSemester(String nim)
    {
        return service.getSemester(nim)
                .map(ApiResponse::getResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<TugasModel>> getTugas(String kd_makul, String kelas)
    {
        return service.getTugas(kd_makul, kelas,"data")
                .map(ApiResponse::getResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<NilaiModel>> getNilai(Map<String, String> param)
    {
        return service.getNilai(param)
                .map(ApiResponse::getResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ApiResponse<List<MessageModel>>> getMessage(String kd_makul, String kelas)
    {
        return service.getMessage(kd_makul, kelas)
                .map(d->d)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    public Observable<ApiResponse> addMessage(HashMap<String, String> param)
    {
        return service.addMessage(param)
                .map(d->d)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ApiResponse> upMateri(Map<String, RequestBody> param, MultipartBody.Part file)
    {
        return service.upMateri(param, file)
                .map(d->d)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ApiResponse> upTugas(Map<String, RequestBody> param, MultipartBody.Part file)
    {
        return service.upTugas(param, file)
                .map(d->d)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
