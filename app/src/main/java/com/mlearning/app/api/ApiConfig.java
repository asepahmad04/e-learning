package com.mlearning.app.api;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by asepahmad
 * Date: 3/12/19
 * Time: 00:13
 * Project: emaklone
 */
public class ApiConfig {

    private static Retrofit retrofit;

    public static Retrofit getRetrofit(String base_url)
    {
        if (retrofit == null)
        {
            GsonBuilder builder = new GsonBuilder();
            builder.setLenient();
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return retrofit;
    }

    private static OkHttpClient getOkHttpClient()
    {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(logging)
                .cache(null)
                .build();
    }
}
