package com.mlearning.app.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asepahmad
 * Date: 4/12/19
 * Project: ELearningAkakom
 */
public class ApiResponse<T> {
    public boolean status;
    public String message;
    @SerializedName("response") T response;

    public T getResponse()
    {
        return response;
    }

}
