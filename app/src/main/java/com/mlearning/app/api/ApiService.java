package com.mlearning.app.api;

import com.mlearning.app.models.MakulModel;
import com.mlearning.app.models.MateriModel;
import com.mlearning.app.models.MessageModel;
import com.mlearning.app.models.NilaiModel;
import com.mlearning.app.models.SemesterModel;
import com.mlearning.app.models.TugasModel;
import com.mlearning.app.models.UserModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;

/**
 * Created by asepahmad
 * Date: 3/13/19
 * Project: papua
 */
public interface ApiService {

    String url = ApiEndpoints.BASE_URL+ApiEndpoints.URL_MATERI +"?type=file&filename=SK.pdf";
    @GET(ApiEndpoints.URL_FILE_MATERI)
    @Streaming
    Call<ResponseBody> downloadFile(@Path("filename") String filename);

    @GET(ApiEndpoints.URL_MATERI)
    Observable<ApiResponse<List<MateriModel>>> getMateri(@Query("kd_makul")String kd_makul, @Query("kelas")String kelas, @Query("type")String type);

    @GET(ApiEndpoints.URL_GET_TUGAS)
    Observable<ApiResponse<List<TugasModel>>> getTugas(@Query("kd_makul")String kd_makul, @Query("kelas")String kelas,  @Query("type")String type);

    @GET(ApiEndpoints.URL_GET_NILAI)
    Observable<ApiResponse<List<NilaiModel>>> getNilai(@QueryMap Map<String, String> param);

    @FormUrlEncoded
    @POST(ApiEndpoints.URL_LOGIN)
    Observable<ApiResponse<UserModel>> actLogin(@Field("username") String username,
                                               @Field("password") String password,
                                               @Field("status") String status);
    @FormUrlEncoded
    @POST(ApiEndpoints.URL_MATERI_DOSEN)
    Observable<ApiResponse<List<MateriModel>>> getMateriDosen(@Field("nip") String nip,
                                                @Field("kd_mk") String kd_mk);

    @FormUrlEncoded
    @POST(ApiEndpoints.URL_TUGAS_DOSEN)
    Observable<ApiResponse<List<TugasModel>>> getTugasDosen(@Field("nip") String nip,
                                                              @Field("kd_mk") String kd_mk);

    @GET(ApiEndpoints.URL_GET_MAKUL)
    Observable<ApiResponse<List<MakulModel>>> getMakul(@QueryMap Map<String, String> param);

    @GET(ApiEndpoints.URL_GET_MAKUL)
    Observable<ApiResponse<List<MakulModel>>> getMakulBySmt(@Query("nim")String nim, @Query("semester")String semester);

    @GET(ApiEndpoints.URL_GET_SEMESTER)
    Observable<ApiResponse<List<SemesterModel>>> getSemester(@Query("nim")String nim);

    @FormUrlEncoded
    @POST(ApiEndpoints.URL_GET_MESSAGE)
    Observable<ApiResponse<List<MessageModel>>> getMessage(@Field("kd_makul") String kd_matkul, @Field("kelas") String kelas);

    @FormUrlEncoded
    @POST(ApiEndpoints.URL_ADD_MESSAGE)
    Observable<ApiResponse> addMessage(@FieldMap HashMap<String, String> data);

    @Multipart
    @POST(ApiEndpoints.URL_MATERI)
    Observable<ApiResponse> upMateri(@PartMap() Map<String, RequestBody> partMap,
                                     @Part MultipartBody.Part file);

    @Multipart
    @POST(ApiEndpoints.URL_GET_TUGAS)
    Observable<ApiResponse> upTugas(@PartMap() Map<String, RequestBody> partMap,
                                     @Part MultipartBody.Part file);


}
