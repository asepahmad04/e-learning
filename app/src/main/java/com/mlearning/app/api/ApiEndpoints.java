package com.mlearning.app.api;

public class ApiEndpoints {
    public static final String BASE_URL = "http://m-learningakakom.000webhostapp.com/api2/";
    public static final String BASE_FILE = "http://m-learningakakom.000webhostapp.com/uploads/";


    static final String URL_MATERI = "materi";
    static final String URL_MATERI_DOSEN = "materi/get_all_by_nip";
    static final String URL_TUGAS_DOSEN = "tugas/get_all_by_nip";
    public static final String URL_LOGIN = "login";
    public static final String URL_GET_MAKUL = "matakuliah";
    public static final String URL_GET_SEMESTER = "semester";
    public static final String URL_GET_TUGAS = "tugas";
    public static final String URL_GET_NILAI = "nilai";
    public static final String URL_FILE_MATERI = "materi/{filename}";
    public static final String URL_GET_MESSAGE = "chat/";
    public static final String URL_ADD_MESSAGE = "chat/add";
}
