package com.mlearning.app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mlearning.app.R;
import com.mlearning.app.models.MateriModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asepahmad
 * Date: 3/27/19
 * Project: ELearningAkakom
 */
public class MateriAdapter extends RecyclerView.Adapter<MateriAdapter.VHolder> {

    private Context context;
    private List<MateriModel> item;
    private OnItemClickListener onItemClickListener;

    public MateriAdapter(Context context, List<MateriModel> item){
        this.context = context;
        this.item = item;
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_materi, viewGroup, false);
        return new VHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder h, int position) {
        MateriModel m = item.get(h.getAdapterPosition());
        h.tvJudul.setText(m.getNamaMateri());
        h.tvKdMakul.setText(m.getKodeMk());
        h.tvNamaFile.setText(m.getNamaFile());
        h.root.setOnClickListener(v -> {
            if(onItemClickListener!=null)
                onItemClickListener.onItemClick(m);
        });

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public void setItem(List<MateriModel> data){
        if(item!=null && item.size() > 0) item.clear();
        item.addAll(data);
        notifyDataSetChanged();
    }

    public class VHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.root)
        View root;
        @BindView(R.id.tvJudul)
        TextView tvJudul;
        @BindView(R.id.tvKodeMakul)
        TextView tvKdMakul;
        @BindView(R.id.tvNamaFile)
        TextView tvNamaFile;

        public VHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener{
        void onItemClick(MateriModel model);
    }
}
