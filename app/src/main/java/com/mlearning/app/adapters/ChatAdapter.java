package com.mlearning.app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mlearning.app.R;
import com.mlearning.app.db.PrefManager;
import com.mlearning.app.models.MessageModel;
import com.mlearning.app.models.MessageType;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mlearning.app.db.PrefManager.PREF_KEY_ID;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<MessageModel> messages;
    private Context context;
    private PrefManager prefManager;

    public ChatAdapter(Context context, List<MessageModel> items){
        this.context = context;
        this.messages = items;
        prefManager = new PrefManager(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //kondisi layout yang akan dipilih sebagai (pengirim/penerima)
        if (viewType == MessageType.SENT_TEXT) {
            return new SentTextHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sent_message_text, parent, false));
        } else {
            return new ReceivedTextHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_received_message_text, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mHolder, int position) {
        //set data pada view berdasarkan kondisi (pengirim/penerima)
        int type = getItemViewType(position);
        MessageModel message = messages.get(position);
        if (type == MessageType.SENT_TEXT) {
            SentTextHolder holder = (SentTextHolder) mHolder;
            holder.tvTime.setText(message.getTime());
            holder.tvMessageContent.setText(message.konten);

        }  else if (type == MessageType.RECEIVED_TEXT) {
            ReceivedTextHolder holder = (ReceivedTextHolder) mHolder;
            holder.tvTime.setText(message.getTime());
            holder.tvMessageContent.setText(message.konten);
            if(message.status.equals("M"))
                holder.tvUsername.setText(message.nama_mhs);
            else holder.tvUsername.setText(String.format(Locale.US, "%s (dosen)", message.nama_dosen));


        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        //kondisi untuk menampilkan view, jika pada list chat data id sama dengan id pada session maka ditampilkan view sebagai pengirim
        String userID = prefManager.getUserDetails().get(PREF_KEY_ID);
        MessageModel message = messages.get(position);
        String id = "M".equals(message.status) ? message.nim : message.npp_dosen;

        if (userID.equals(id)) {
            return MessageType.SENT_TEXT;
        } else {
            return MessageType.RECEIVED_TEXT;
        }
    }

    class SentTextHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_message_content)
        TextView tvMessageContent;

        public SentTextHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_time)
        TextView tvTime;

        public ReceivedMessageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ReceivedTextHolder extends ReceivedMessageHolder {
        @BindView(R.id.tv_message_content)
        TextView tvMessageContent;
        @BindView(R.id.container)
        FrameLayout container;

        public ReceivedTextHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void addLast(MessageModel model){
        messages.add(model);
        notifyItemInserted(messages.size() - 1);
    }
}
