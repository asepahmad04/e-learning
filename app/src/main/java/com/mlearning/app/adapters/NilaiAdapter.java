package com.mlearning.app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mlearning.app.R;
import com.mlearning.app.models.NilaiModel;
import com.mlearning.app.util.Constans;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by asepahmad
 * Date: 3/27/19
 * Project: ELearningAkakom
 */
public class NilaiAdapter extends RecyclerView.Adapter<NilaiAdapter.VHolder> {

    private Context context;
    private List<NilaiModel> item;
    private OnItemClickListener onItemClickListener;
    private String tipe;

    public NilaiAdapter(Context context, List<NilaiModel> item, String tipe){
        this.context = context;
        this.item = item;
        this.tipe = tipe;
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_nilai, viewGroup, false);
        return new VHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder h, int position) {
        NilaiModel m = item.get(h.getAdapterPosition());
        if(tipe.equals(Constans.STATUS_MHS)){
            h.tvJudul.setText(m.nama_tugas);
            h.tvNilai.setText(m.getNilai());
            h.tvTgl.setText(m.getTgl());
        }else {
            h.tvJudul.setText(m.getNim());
            h.tvNilai.setText(m.getNilai());
            h.tvTgl.setText(m.getTgl());
        }
        h.root.setOnClickListener(v -> {
            if(onItemClickListener!=null)
                onItemClickListener.onItemClick(m);
        });

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public void setItem(List<NilaiModel> data){
        if(item!=null && item.size() > 0) item.clear();
        item.addAll(data);
        notifyDataSetChanged();
    }

    public class VHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.root)
        View root;
        @BindView(R.id.tvNama)
        TextView tvJudul;
        @BindView(R.id.tvNilai)
        TextView tvNilai;
        @BindView(R.id.tvTgl)
        TextView tvTgl;

        public VHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener{
        void onItemClick(NilaiModel model);
    }
}
